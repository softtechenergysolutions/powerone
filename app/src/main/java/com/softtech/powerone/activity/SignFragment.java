package com.softtech.powerone.activity;


import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kyanogen.signatureview.SignatureView;
import com.softtech.powerone.R;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import static com.softtech.powerone.activity.HomeScreenActivity.btnCust;
import static com.softtech.powerone.activity.HomeScreenActivity.btnEngr;
import static com.softtech.powerone.activity.HomeScreenActivity.showImage;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignFragment extends DialogFragment {
    SignatureView signatureView;
    Dialog dialog;
    public SignFragment() {
        // Required empty public constructor
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

             dialog = null;
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            View v = getActivity().getLayoutInflater().inflate(R.layout.signingpad, null);
            signatureView=v.findViewById(R.id.signature_view);
        v.findViewById(R.id.btnSaveSig).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap=signatureView.getSignatureBitmap();

                SaveImage(bitmap);
                dialog.dismiss();

            }
        });

        v.findViewById(R.id.btnClear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                signatureView.clearCanvas();
            }
        });
            builder.setView(v);
            dialog = builder.create();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            return dialog;

    }


    private  void SaveImage(Bitmap finalBitmap) {

        if(btnEngr==true) {

            String root = Environment.getExternalStorageDirectory().getAbsolutePath();
            File myDir = new File(root + "/powerone");
            myDir.mkdirs();

            String fname = "engineer.jpg";
            File file = new File(myDir, fname);
            if (file.exists()) file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
                //Toast.makeText(getActivity(), "Signature Saved", Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if(btnCust==true){


            String root = Environment.getExternalStorageDirectory().getAbsolutePath();
            File myDir = new File(root + "/powerone");
            myDir.mkdirs();

            String fname = "customer.jpg";
            File file = new File(myDir, fname);
            if (file.exists()) file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
               // Toast.makeText(getActivity(), "Signature Saved", Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        showImage();


    }
}

package com.softtech.powerone.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.softtech.powerone.ApplicationActivity;
import com.softtech.powerone.R;
import com.softtech.powerone.model.DBHelper;
import com.softtech.powerone.model.ExportDataBean;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;


public class EmailAttachmentActivity extends AppCompatActivity implements View.OnClickListener {
    // Storage Permissions
    Dialog logoutDialog;
    Button logoutOk, logoutCancel;
    String to,cc,subject,contents,path;
    DBHelper dbHelper;
    Cursor cursor;
    EditText editCC,editSub,editcontent;
    AutoCompleteTextView editTO;
    ArrayList<String> arrayListMail;
    TextView editAttach;
    Button btnSend;
    String pdf_name;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    ExportDataBean exportDataBean;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_attachment);

        init();

        verifyStoragePermissions(this);
        // Storage Permissions


    }

    public void getMails()
    {

        if (cursor.moveToFirst()) {

            do {

                String email_id = cursor.getString(cursor.getColumnIndex("email"));
                arrayListMail.add(email_id);
                Log.v("email",email_id);

            }while (cursor.moveToNext());
        }

    }

    public void init()
    {
        Bundle bundle=getIntent().getExtras();
        exportDataBean= (ExportDataBean) bundle.getSerializable("exportDataBean");
        String customer=exportDataBean.getCustomer();
        String firstThree=customer.substring(0,3);
        pdf_name=firstThree+"_"+exportDataBean.getSerialNo();

        btnSend=findViewById(R.id.btn_send);
        editTO=findViewById(R.id.editTO);
        editCC=findViewById(R.id.editCC);
        editSub=findViewById(R.id.editSubject);
        editAttach=findViewById(R.id.editAttachement);
        editcontent=findViewById(R.id.editContents);
        editAttach.setText(pdf_name+".pdf");
        editSub.setText(pdf_name+".pdf");
        arrayListMail=new ArrayList<>();
        dbHelper=new DBHelper(this);
        cursor= dbHelper.get_mailid();
        getMails();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, arrayListMail);
        editTO.setAdapter(adapter);
        btnSend.setOnClickListener(this);




    }




    public void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(activity,
                        PERMISSIONS_STORAGE,
                        REQUEST_EXTERNAL_STORAGE);

            }


        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    createPDF();
                }
            }).start();

        }

    }

    public void createPDF(){
        //create document object
        Document document=new Document();

        //output file path
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/POWERONE/PDF");
        myDir.mkdirs();
      //  File file = new File(myDir, fname);

        String outpath= Environment.getExternalStorageDirectory()+"/POWERONE/PDF/"+pdf_name+".pdf";

        try {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(outpath));
            document.open();

            InputStream full = getAssets().open("srv.png");
            Bitmap bmpfull = BitmapFactory.decodeStream(full);
            ByteArrayOutputStream streamFull = new ByteArrayOutputStream();
            bmpfull.compress(Bitmap.CompressFormat.PNG, 100, streamFull);
            Image imageFull = Image.getInstance(streamFull.toByteArray());

            imageFull.scaleToFit(850, 815);
            // imageFull.setDpi(140,60);

            imageFull.setAbsolutePosition(0, 0);

            document.add(imageFull);


            Font smallfont = new Font(Font.FontFamily.HELVETICA, 10);


            // Setting paragraph line spacing to 32
            Paragraph para1 = new Paragraph(32);
            para1.setFont(smallfont);
            para1.setSpacingBefore(162);
            para1.setIndentationLeft(100);
            para1.setSpacingAfter(-7);
            para1.add(new Chunk(exportDataBean.getCustomer()));
            document.add(para1);


            Paragraph para2 = new Paragraph();
            para2.setFont(smallfont);
            para2.setIndentationLeft(340);
            para2.setSpacingAfter(8);
            para2.add(new Chunk(exportDataBean.getPeriod()));
            document.add(para2);


            Paragraph para3 = new Paragraph();
            para3.setFont(smallfont);
            para3.setIndentationLeft(340);
            para3.setSpacingAfter(-10);
            para3.add(new Chunk(exportDataBean.getServiceType()));
            document.add(para3);


            Paragraph para4 = new Paragraph();
            para4.setFont(smallfont);
            para4.setIndentationLeft(100);
            para4.setSpacingAfter(-5);
            para4.add(new Chunk(exportDataBean.getAddress()));
            document.add(para4);


            Paragraph para5 = new Paragraph();
            para5.setFont(smallfont);
            para5.setIndentationLeft(100);
            para5.setSpacingAfter(10);
            para5.add(new Chunk(" "));
            document.add(para5);


            Paragraph para6 = new Paragraph();
            para6.setFont(smallfont);
            para6.setIndentationLeft(100);
            para6.setSpacingAfter(13);
            para6.add(new Chunk(exportDataBean.getDate()));
            document.add(para6);


            Paragraph para7 = new Paragraph();
            para7.setFont(smallfont);
            para7.setIndentationLeft(130);
            para7.setSpacingAfter(8);
            para7.add(new Chunk(exportDataBean.getCphone()));
            document.add(para7);


            Paragraph para8 = new Paragraph();
            para8.setFont(smallfont);
            para8.setIndentationLeft(130);
            para8.setSpacingAfter(10);
            para8.add(new Chunk(exportDataBean.getCname()));
            document.add(para8);

            Paragraph para9 = new Paragraph();
            para9.setFont(smallfont);
            para9.setIndentationLeft(130);
            para9.setSpacingAfter(10);
            para9.add(new Chunk(exportDataBean.getEngineerName()));
            document.add(para9);


            Paragraph para10 = new Paragraph();
            para10.setFont(smallfont);
            para10.setIndentationLeft(130);
            para10.setSpacingAfter(10);
            para10.add(new Chunk(exportDataBean.getEngineerPhone()));
            document.add(para10);


            Paragraph para11 = new Paragraph();
            para11.setFont(smallfont);
            para11.setIndentationLeft(130);
            para11.setSpacingAfter(5);
            para11.add(new Chunk(exportDataBean.getComplaintNo()));
            document.add(para11);


            Paragraph para12 = new Paragraph();
            para12.setFont(smallfont);
            para12.setIndentationLeft(375);
            para12.setSpacingAfter(7);
            para12.add(new Chunk(exportDataBean.getInputVoltage2()));
            document.add(para12);


            Paragraph para13 = new Paragraph();
            para13.setFont(smallfont);
            para13.setIndentationLeft(375);
            para13.add(new Chunk(exportDataBean.getInputNEvoltage()));
            para13.setSpacingAfter(-10);
            document.add(para13);

            Paragraph para14 = new Paragraph();
            para14.setFont(smallfont);
            para14.setIndentationLeft(130);
            para14.setSpacingAfter(2);
            para14.add(new Chunk(exportDataBean.getCapacity()));
            document.add(para14);


            Paragraph para15 = new Paragraph();
            para15.setFont(smallfont);
            para15.setIndentationLeft(375);
            para15.add(new Chunk(exportDataBean.getDcVoltage()));
            para15.setSpacingAfter(-8);
            document.add(para15);

            Paragraph para16 = new Paragraph();
            para16.setFont(smallfont);
            para16.setIndentationLeft(130);
            para16.setSpacingAfter(-2);
            para16.add(new Chunk(exportDataBean.getModel()));
            document.add(para16);


            Paragraph para17 = new Paragraph();
            para17.setFont(smallfont);
            para17.setIndentationLeft(375);
            para17.add(new Chunk(exportDataBean.getDcCurrent()));
            para17.setSpacingAfter(-5);
            document.add(para17);


            Paragraph para18 = new Paragraph();
            para18.setFont(smallfont);
            para18.setIndentationLeft(130);
            para18.setSpacingAfter(-4);
            para18.add(new Chunk(exportDataBean.getSerialNo()));
            document.add(para18);


            Paragraph para19 = new Paragraph();
            para19.setFont(smallfont);
            para19.setIndentationLeft(375);
            para19.add(new Chunk(exportDataBean.getOutputVoltage()));
            para19.setSpacingAfter(-2);
            document.add(para19);


            Paragraph para20 = new Paragraph();
            para20.setFont(smallfont);
            para20.setIndentationLeft(130);
            para20.setSpacingAfter(-9);
            para20.add(new Chunk(exportDataBean.getType()));
            document.add(para20);


            Paragraph para21 = new Paragraph();
            para21.setFont(smallfont);
            para21.setIndentationLeft(375);
            para21.setSpacingAfter(6);
            para21.add(new Chunk(exportDataBean.getOutputNEvoltage()));
            document.add(para21);


            Paragraph para22 = new Paragraph();
            para22.setFont(smallfont);
            para22.setIndentationLeft(130);
            para22.setSpacingAfter(-13);
            para22.add(new Chunk(exportDataBean.getInputVoltage()));
            document.add(para22);


            Paragraph para23 = new Paragraph();
            para23.setFont(smallfont);
            para23.setIndentationLeft(375);
            para23.setSpacingAfter(40);
            para23.add(new Chunk(exportDataBean.getOutputCurrent()));
            document.add(para23);


            Paragraph para24 = new Paragraph();
            para24.setFont(smallfont);
            para24.setIndentationLeft(70);
            para24.add(new Chunk(exportDataBean.getRemark()));
            document.add(para24);


            try {
                String photoPath = Environment.getExternalStorageDirectory() + "/powerone/engineer.jpg";
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bmp = BitmapFactory.decodeFile(photoPath, options);

                //InputStream ims = con.getAssets().open("prof_image.png");
                //Bitmap bmp = BitmapFactory.decodeStream(ims);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                Image image = Image.getInstance(stream.toByteArray());

                image.scaleToFit(50, 30);
                // imageFull.setDpi(140,60);

                image.setAbsolutePosition(90, 80);

                document.add(image);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            try {
                String photoPath = Environment.getExternalStorageDirectory() + "/powerone/customer.jpg";
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bmp = BitmapFactory.decodeFile(photoPath, options);

                //InputStream ims = con.getAssets().open("prof_image.png");
                //Bitmap bmp = BitmapFactory.decodeStream(ims);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                Image image = Image.getInstance(stream.toByteArray());

                image.scaleToFit(50, 30);
                // imageFull.setDpi(140,60);

                image.setAbsolutePosition(430, 80);

                document.add(image);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            if (exportDataBean.iseConnection() == true) {
                InputStream chkboxstream = getAssets().open("checked.png");
                Bitmap bmpcheck = BitmapFactory.decodeStream(chkboxstream);
                ByteArrayOutputStream streamcheck = new ByteArrayOutputStream();
                bmpcheck.compress(Bitmap.CompressFormat.PNG, 100, streamcheck);
                Image imagecheck = Image.getInstance(streamcheck.toByteArray());

                imagecheck.scaleToFit(20, 20);
                // imageFull.setDpi(140,60);

                imagecheck.setAbsolutePosition(500, 543);

                document.add(imagecheck);
            } else {
                InputStream chkboxstream = getAssets().open("unchecked.png");
                Bitmap bmpcheck = BitmapFactory.decodeStream(chkboxstream);
                ByteArrayOutputStream streamcheck = new ByteArrayOutputStream();
                bmpcheck.compress(Bitmap.CompressFormat.PNG, 100, streamcheck);
                Image imagecheck = Image.getInstance(streamcheck.toByteArray());

                imagecheck.scaleToFit(20, 20);
                // imageFull.setDpi(140,60);

                imagecheck.setAbsolutePosition(500, 543);

                document.add(imagecheck);
            }


            if (exportDataBean.isOverHeating() == true)
            {InputStream chkboxstream2 = getAssets().open("checked.png");
            Bitmap bmpcheck2 = BitmapFactory.decodeStream(chkboxstream2);
            ByteArrayOutputStream streamcheck2 = new ByteArrayOutputStream();
            bmpcheck2.compress(Bitmap.CompressFormat.PNG, 100, streamcheck2);
            Image imagecheck2 = Image.getInstance(streamcheck2.toByteArray());

            imagecheck2.scaleToFit(20, 20);
            // imageFull.setDpi(140,60);

            imagecheck2.setAbsolutePosition(500, 525);

            document.add(imagecheck2);
        }
        else
            {
                InputStream chkboxstream2 = getAssets().open("unchecked.png");
                Bitmap bmpcheck2 = BitmapFactory.decodeStream(chkboxstream2);
                ByteArrayOutputStream streamcheck2 = new ByteArrayOutputStream();
                bmpcheck2.compress(Bitmap.CompressFormat.PNG, 100, streamcheck2);
                Image imagecheck2 = Image.getInstance(streamcheck2.toByteArray());

                imagecheck2.scaleToFit(20, 20);
                // imageFull.setDpi(140,60);

                imagecheck2.setAbsolutePosition(500, 525);

                document.add(imagecheck2);
            }


            if(exportDataBean.isCoolingFan()==true) {
                InputStream chkboxstream3 = getAssets().open("checked.png");
                Bitmap bmpcheck3 = BitmapFactory.decodeStream(chkboxstream3);
                ByteArrayOutputStream streamcheck3 = new ByteArrayOutputStream();
                bmpcheck3.compress(Bitmap.CompressFormat.PNG, 100, streamcheck3);
                Image imagecheck3 = Image.getInstance(streamcheck3.toByteArray());

                imagecheck3.scaleToFit(20, 20);
                // imageFull.setDpi(140,60);

                imagecheck3.setAbsolutePosition(500, 500);

                document.add(imagecheck3);
            }
            else
            {
                InputStream chkboxstream3 = getAssets().open("unchecked.png");
                Bitmap bmpcheck3 = BitmapFactory.decodeStream(chkboxstream3);
                ByteArrayOutputStream streamcheck3 = new ByteArrayOutputStream();
                bmpcheck3.compress(Bitmap.CompressFormat.PNG, 100, streamcheck3);
                Image imagecheck3 = Image.getInstance(streamcheck3.toByteArray());

                imagecheck3.scaleToFit(20, 20);
                // imageFull.setDpi(140,60);

                imagecheck3.setAbsolutePosition(500, 500);

                document.add(imagecheck3);
            }


            if(exportDataBean.isUPSinternal()==true) {
                InputStream chkboxstream4 = getAssets().open("checked.png");
                Bitmap bmpcheck4 = BitmapFactory.decodeStream(chkboxstream4);
                ByteArrayOutputStream streamcheck4 = new ByteArrayOutputStream();
                bmpcheck4.compress(Bitmap.CompressFormat.PNG, 100, streamcheck4);
                Image imagecheck4 = Image.getInstance(streamcheck4.toByteArray());

                imagecheck4.scaleToFit(20, 20);
                // imageFull.setDpi(140,60);

                imagecheck4.setAbsolutePosition(500, 475);

                document.add(imagecheck4);
            }
            else
            {
                InputStream chkboxstream4 = getAssets().open("unchecked.png");
                Bitmap bmpcheck4 = BitmapFactory.decodeStream(chkboxstream4);
                ByteArrayOutputStream streamcheck4 = new ByteArrayOutputStream();
                bmpcheck4.compress(Bitmap.CompressFormat.PNG, 100, streamcheck4);
                Image imagecheck4 = Image.getInstance(streamcheck4.toByteArray());

                imagecheck4.scaleToFit(20, 20);
                // imageFull.setDpi(140,60);

                imagecheck4.setAbsolutePosition(500, 475);

                document.add(imagecheck4);
            }


            if(exportDataBean.isBatteryTerminal()==true) {
                InputStream chkboxstream5 = getAssets().open("checked.png");
                Bitmap bmpcheck5 = BitmapFactory.decodeStream(chkboxstream5);
                ByteArrayOutputStream streamcheck5 = new ByteArrayOutputStream();
                bmpcheck5.compress(Bitmap.CompressFormat.PNG, 100, streamcheck5);
                Image imagecheck5 = Image.getInstance(streamcheck5.toByteArray());

                imagecheck5.scaleToFit(20, 20);
                // imageFull.setDpi(140,60);

                imagecheck5.setAbsolutePosition(500, 450);

                document.add(imagecheck5);
            }
            else
            {
                InputStream chkboxstream5 = getAssets().open("unchecked.png");
                Bitmap bmpcheck5 = BitmapFactory.decodeStream(chkboxstream5);
                ByteArrayOutputStream streamcheck5 = new ByteArrayOutputStream();
                bmpcheck5.compress(Bitmap.CompressFormat.PNG, 100, streamcheck5);
                Image imagecheck5 = Image.getInstance(streamcheck5.toByteArray());

                imagecheck5.scaleToFit(20, 20);
                // imageFull.setDpi(140,60);

                imagecheck5.setAbsolutePosition(500, 450);

                document.add(imagecheck5);
            }


            if(exportDataBean.isBatteryCondition()==true) {
                InputStream chkboxstream6 = getAssets().open("checked.png");
                Bitmap bmpcheck6 = BitmapFactory.decodeStream(chkboxstream6);
                ByteArrayOutputStream streamcheck6 = new ByteArrayOutputStream();
                bmpcheck6.compress(Bitmap.CompressFormat.PNG, 100, streamcheck6);
                Image imagecheck6 = Image.getInstance(streamcheck6.toByteArray());

                imagecheck6.scaleToFit(20, 20);
                // imageFull.setDpi(140,60);

                imagecheck6.setAbsolutePosition(500, 430);

                document.add(imagecheck6);
            }
            else
            {
                InputStream chkboxstream6 = getAssets().open("unchecked.png");
                Bitmap bmpcheck6 = BitmapFactory.decodeStream(chkboxstream6);
                ByteArrayOutputStream streamcheck6 = new ByteArrayOutputStream();
                bmpcheck6.compress(Bitmap.CompressFormat.PNG, 100, streamcheck6);
                Image imagecheck6 = Image.getInstance(streamcheck6.toByteArray());

                imagecheck6.scaleToFit(20, 20);
                // imageFull.setDpi(140,60);

                imagecheck6.setAbsolutePosition(500, 430);

                document.add(imagecheck6);

            }

            if(exportDataBean.isUPSExternal()==true) {
                InputStream chkboxstream7 = getAssets().open("checked.png");
                Bitmap bmpcheck7 = BitmapFactory.decodeStream(chkboxstream7);
                ByteArrayOutputStream streamcheck7 = new ByteArrayOutputStream();
                bmpcheck7.compress(Bitmap.CompressFormat.PNG, 100, streamcheck7);
                Image imagecheck7 = Image.getInstance(streamcheck7.toByteArray());

                imagecheck7.scaleToFit(20, 20);
                // imageFull.setDpi(140,60);

                imagecheck7.setAbsolutePosition(500, 410);

                document.add(imagecheck7);
            }
            else
            {
                InputStream chkboxstream7 = getAssets().open("unchecked.png");
                Bitmap bmpcheck7 = BitmapFactory.decodeStream(chkboxstream7);
                ByteArrayOutputStream streamcheck7 = new ByteArrayOutputStream();
                bmpcheck7.compress(Bitmap.CompressFormat.PNG, 100, streamcheck7);
                Image imagecheck7 = Image.getInstance(streamcheck7.toByteArray());

                imagecheck7.scaleToFit(20, 20);
                // imageFull.setDpi(140,60);

                imagecheck7.setAbsolutePosition(500, 410);

                document.add(imagecheck7);
            }

            document.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch(IOException ex)
        {
            return;
        }
    }

    public void mail(String to,String cc,String subject,String attach,String contents)
    {
        dbHelper.insertData(to);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
       /* Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("application/pdf");
        shareIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { to });
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        shareIntent.putExtra(Intent.EXTRA_CC, cc);
        shareIntent.putExtra(Intent.EXTRA_TEXT, contents);
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(attach));
        startActivity(shareIntent);*/



        String mailto = "mailto:"+to+
                "?cc=" + cc +
                "&subject=" + Uri.encode(subject) +
                "&body=" + Uri.encode(contents);

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse(mailto));
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(attach));
        startActivity(emailIntent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_send:
                 to=editTO.getText().toString().trim();
                 cc=editCC.getText().toString().trim();
                 subject=editSub.getText().toString().trim();
                 contents=editcontent.getText().toString().trim();
                 path="file:///storage/emulated/0/POWERONE/PDF/"+pdf_name+".pdf";
                if(to.equals(""))
                {
                    ApplicationActivity.getInstance().showSnackToast("Enter To address", EmailAttachmentActivity.this);
                    return;
                }

                if(subject.equals(""))
                {
                    ApplicationActivity.getInstance().showSnackToast("Enter the Subject", EmailAttachmentActivity.this);
                    return;
                }

                if(editAttach.getText().toString().trim().equals(""))
                {
                    ApplicationActivity.getInstance().showSnackToast("Attachment is missed", EmailAttachmentActivity.this);
                    return;
                }

                sendPDF();

                break;

        }

    }

    public void sendPDF() {
        logoutDialog = new Dialog(EmailAttachmentActivity.this);
        logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutDialog.setCancelable(true);
        logoutDialog.setContentView(R.layout.send_dialog);

        logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        logoutOk = (Button) logoutDialog.findViewById(R.id.btnSendYes);
        logoutCancel = (Button) logoutDialog.findViewById(R.id.btnSendNo);

        logoutOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mail(to,cc,subject,path,contents);
                finish();
                logoutDialog.dismiss();


            }
        });

        logoutCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutDialog.dismiss();
            }
        });

        logoutDialog.show();
    }

}

package com.softtech.powerone.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.softtech.powerone.ApplicationActivity;
import com.softtech.powerone.R;
import com.softtech.powerone.model.ExportDataBean;

import java.util.Calendar;

public class HomeScreenActivity extends AppCompatActivity {

    ScrollView scrollView;
    FloatingActionButton floatingActionButton;
    Dialog logoutDialog;
    Button logoutOk, logoutCancel;
    static boolean btnEngr, btnCust;
    TextView txtCalendar,txtTime;
    ImageView imageCalendar,imageTime;
    CircularImageView imageDP;
    static ImageView imageTechEngineerSign, imageCustomersign;
    Button btn_saveNsend;
    static EditText editCName, editServiceEname;
    EditText editCustomer, editAddress, editCcontact, editEngineerContact, editComplaintNo;
    EditText editCapacity, editModel, editSerial, editType, editInputVoltage;
    EditText editPeriod, editServiceType;
    EditText editInputVoltage2, editInputNEVoltage, editDCVoltage, editDCCurrent, editOutputVoltage, editOutputNEVoltage, editOutputCurrent, editRemark;
    CheckBox checkEConnection, checkOverHeating, checkCoolingFan, checkUPSinternal, checkBatteryTerminal, checkBatteryCondition, checkUPsexternal;
    private String customer, address, date, Cname, Cphone, engineerName, engineerPhone, complaintNo, capacity, model, serialNo, type;
    private String inputVoltage, period, serviceType, inputVoltage2, inputNEvoltage, dcVoltage, dcCurrent, outputVoltage, outputNEvoltage, outputCurrent;
    private String remark;
    boolean eConnection, overHeating, coolingFan, UPSinternal, batteryTerminal, batteryCondition, UPSExternal;


    GoogleApiClient mGoogleApiClient;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        String image_url=getIntent().getExtras().getString("image_url");
        //Log.v("image_url",image_url);
        init();

        if (image_url != null) {
            Glide.with(getApplicationContext()).load(image_url)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageDP);

        }


        scrollView.setSmoothScrollingEnabled(true);



        imageTechEngineerSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnEngr = true;
                SignFragment signFragment2 = new SignFragment();
                signFragment2.show(getSupportFragmentManager(), null);

            }
        });

        imageCustomersign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnCust = true;
                SignFragment signFragment3 = new SignFragment();
                signFragment3.show(getSupportFragmentManager(), null);

            }
        });


        checkEConnection.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    eConnection = true;
                    Log.v("dataa", "" + eConnection);
                } else {
                    eConnection = false;
                    Log.v("dataa", "" + eConnection);

                }
            }
        });


        checkOverHeating.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    overHeating = true;
                    Log.v("dataa", "" + overHeating);
                } else {
                    overHeating = false;
                    Log.v("dataa", "" + overHeating);

                }
            }
        });


        checkCoolingFan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    coolingFan = true;
                    Log.v("dataa", "" + coolingFan);
                } else {
                    coolingFan = false;
                    Log.v("dataa", "" + coolingFan);

                }
            }
        });


        checkUPSinternal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    UPSinternal = true;
                    Log.v("dataa", "" + UPSinternal);
                } else {
                    UPSinternal = false;
                    Log.v("dataa", "" + UPSinternal);

                }
            }
        });


        checkBatteryTerminal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    batteryTerminal = true;
                    Log.v("dataa", "" + batteryTerminal);
                } else {
                    batteryTerminal = false;
                    Log.v("dataa", "" + batteryTerminal);

                }
            }
        });


        checkBatteryCondition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    batteryCondition = true;
                    Log.v("dataa", "" + batteryCondition);
                } else {
                    batteryCondition = false;
                    Log.v("dataa", "" + batteryCondition);

                }
            }
        });


        checkUPsexternal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    UPSExternal = true;
                    Log.v("dataa", "" + UPSExternal);
                } else {
                    UPSExternal = false;
                    Log.v("dataa", "" + UPSExternal);

                }
            }
        });


        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Handle the click.

                customer = editCustomer.getText().toString().trim();
                address = editAddress.getText().toString().trim();
                Cname = editCName.getText().toString().trim();
                Cphone = editCcontact.getText().toString().trim();
                customer = editCustomer.getText().toString().trim();
                engineerName = editServiceEname.getText().toString().trim();
                engineerPhone = editEngineerContact.getText().toString().trim();
                complaintNo = editComplaintNo.getText().toString().trim();
                capacity = editCapacity.getText().toString().trim() + " KVA";
                model = editModel.getText().toString().trim();
                serialNo = editSerial.getText().toString().trim();
                type = editType.getText().toString().trim();
                inputVoltage = editInputVoltage.getText().toString().trim() + " V";
                period = editPeriod.getText().toString().trim();
                serviceType = editServiceType.getText().toString().trim();
                inputVoltage2 = editInputVoltage2.getText().toString().trim() + " V";
                inputNEvoltage = editInputNEVoltage.getText().toString().trim() + " V";
                dcVoltage = editDCVoltage.getText().toString().trim() + " V";
                dcCurrent = editDCCurrent.getText().toString().trim() + " A";
                outputVoltage = editOutputVoltage.getText().toString().trim() + " V";
                outputNEvoltage = editOutputNEVoltage.getText().toString().trim() + " V";
                outputCurrent = editOutputCurrent.getText().toString().trim() + " A";
                remark = editRemark.getText().toString().trim();
                date = txtCalendar.getText().toString().trim()+" "+txtTime.getText().toString().trim();


                if (customer.length() < 3) {
                    ApplicationActivity.getInstance().showSnackToast("Customer name is Invalid", HomeScreenActivity.this);
                    editCustomer.requestFocus();
                    return;
                }

                if (customer.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Customer", HomeScreenActivity.this);
                    editCustomer.requestFocus();
                    return;
                }

                if (address.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Address", HomeScreenActivity.this);
                    editAddress.requestFocus();
                    return;
                }

                if (date.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Select date", HomeScreenActivity.this);
                    return;
                }


                if (Cname.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Customer name", HomeScreenActivity.this);
                    editCName.requestFocus();
                    return;
                }

                if (Cphone.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Customer contact", HomeScreenActivity.this);
                    editCcontact.requestFocus();
                    return;
                }

                if (engineerName.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Engineer name", HomeScreenActivity.this);
                    editServiceEname.requestFocus();
                    return;
                }

                if (engineerPhone.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Engineer contact", HomeScreenActivity.this);
                    editEngineerContact.requestFocus();
                    return;
                }


                if (complaintNo.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Complaint no", HomeScreenActivity.this);
                    editComplaintNo.requestFocus();
                    return;
                }


                if (capacity.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Capacity", HomeScreenActivity.this);
                    editCapacity.requestFocus();
                    return;
                }

                if (model.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Model", HomeScreenActivity.this);
                    editModel.requestFocus();
                    return;
                }

                if (serialNo.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Serial no", HomeScreenActivity.this);
                    editSerial.requestFocus();
                    return;
                }


                if (type.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Type", HomeScreenActivity.this);
                    editType.requestFocus();
                    return;
                }


                if (inputVoltage.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Input Voltage", HomeScreenActivity.this);
                    editInputVoltage.requestFocus();
                    return;
                }


                if (period.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Period", HomeScreenActivity.this);
                    editPeriod.requestFocus();
                    return;
                }


                if (serviceType.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Service Type", HomeScreenActivity.this);
                    editServiceType.requestFocus();
                    return;
                }


                if (inputVoltage2.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Input Voltage", HomeScreenActivity.this);
                    editInputVoltage2.requestFocus();
                    return;
                }


                if (dcVoltage.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter DC Voltage", HomeScreenActivity.this);
                    editDCVoltage.requestFocus();
                    return;
                }

                if (outputVoltage.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Output Voltage", HomeScreenActivity.this);
                    editOutputVoltage.requestFocus();
                    return;
                }


                if (outputNEvoltage.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Output N-E Voltage", HomeScreenActivity.this);
                    editOutputNEVoltage.requestFocus();
                    return;
                }

                if (outputCurrent.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Output Current", HomeScreenActivity.this);
                    editDCCurrent.requestFocus();
                    return;
                }


                if (remark.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Remark", HomeScreenActivity.this);
                    editRemark.requestFocus();
                    return;
                }



                prepareNsend();

            }
        });

        btn_saveNsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                customer = editCustomer.getText().toString().trim();
                address = editAddress.getText().toString().trim();
                Cname = editCName.getText().toString().trim();
                Cphone = editCcontact.getText().toString().trim();
                customer = editCustomer.getText().toString().trim();
                engineerName = editServiceEname.getText().toString().trim();
                engineerPhone = editEngineerContact.getText().toString().trim();
                complaintNo = editComplaintNo.getText().toString().trim();
                capacity = editCapacity.getText().toString().trim() + " KVA";
                model = editModel.getText().toString().trim();
                serialNo = editSerial.getText().toString().trim();
                type = editType.getText().toString().trim();
                inputVoltage = editInputVoltage.getText().toString().trim() + " V";
                period = editPeriod.getText().toString().trim();
                serviceType = editServiceType.getText().toString().trim();
                inputVoltage2 = editInputVoltage2.getText().toString().trim() + " V";
                inputNEvoltage = editInputNEVoltage.getText().toString().trim() + " V";
                dcVoltage = editDCVoltage.getText().toString().trim() + " V";
                dcCurrent = editDCCurrent.getText().toString().trim() + " A";
                outputVoltage = editOutputVoltage.getText().toString().trim() + " V";
                outputNEvoltage = editOutputNEVoltage.getText().toString().trim() + " V";
                outputCurrent = editOutputCurrent.getText().toString().trim() + " A";
                remark = editRemark.getText().toString().trim();
                date = txtCalendar.getText().toString().trim();


                if (customer.length() < 3) {
                    ApplicationActivity.getInstance().showSnackToast("Customer name is Invalid", HomeScreenActivity.this);
                    editCustomer.requestFocus();
                    return;
                }



                if (customer.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Customer", HomeScreenActivity.this);
                    editCustomer.requestFocus();
                    return;
                }

                if (address.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Address", HomeScreenActivity.this);
                    editAddress.requestFocus();
                    return;
                }

                if (date.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Select date", HomeScreenActivity.this);
                    return;
                }


                if (Cname.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Customer name", HomeScreenActivity.this);
                    editCName.requestFocus();
                    return;
                }

                if (Cphone.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Customer contact", HomeScreenActivity.this);
                    editCcontact.requestFocus();
                    return;
                }

                if (engineerName.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Engineer name", HomeScreenActivity.this);
                    editServiceEname.requestFocus();
                    return;
                }

                if (engineerPhone.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Engineer contact", HomeScreenActivity.this);
                    editEngineerContact.requestFocus();
                    return;
                }


                if (complaintNo.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Complaint no", HomeScreenActivity.this);
                    editComplaintNo.requestFocus();
                    return;
                }


                if (capacity.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Capacity", HomeScreenActivity.this);
                    editCapacity.requestFocus();
                    return;
                }

                if (model.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Model", HomeScreenActivity.this);
                    editModel.requestFocus();
                    return;
                }

                if (serialNo.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Serial no", HomeScreenActivity.this);
                    editSerial.requestFocus();
                    return;
                }


                if (type.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Type", HomeScreenActivity.this);
                    editType.requestFocus();
                    return;
                }


                if (inputVoltage.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Input Voltage", HomeScreenActivity.this);
                    editInputVoltage.requestFocus();
                    return;
                }


                if (period.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Period", HomeScreenActivity.this);
                    editPeriod.requestFocus();
                    return;
                }


                if (serviceType.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Service Type", HomeScreenActivity.this);
                    editServiceType.requestFocus();
                    return;
                }


                if (inputVoltage2.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Input Voltage", HomeScreenActivity.this);
                    editInputVoltage2.requestFocus();
                    return;
                }


                if (dcVoltage.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter DC Voltage", HomeScreenActivity.this);
                    editDCVoltage.requestFocus();
                    return;
                }

                if (outputVoltage.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Output Voltage", HomeScreenActivity.this);
                    editOutputVoltage.requestFocus();
                    return;
                }


                if (outputNEvoltage.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Output N-E Voltage", HomeScreenActivity.this);
                    editOutputNEVoltage.requestFocus();
                    return;
                }

                if (outputCurrent.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Output Current", HomeScreenActivity.this);
                    editDCCurrent.requestFocus();
                    return;
                }


                if (remark.equals("")) {
                    ApplicationActivity.getInstance().showSnackToast("Enter Remark", HomeScreenActivity.this);
                    editRemark.requestFocus();
                    return;
                }



                prepareNsend();



            }
        });


        imageTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(HomeScreenActivity.this, R.style.MyDatePickerDialogTheme ,new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        txtTime.setText( selectedHour + ":" + selectedMinute);

                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();



            }
        });


        imageCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Dialog dialog = null;
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int date = calendar.get((Calendar.DATE));

                DatePickerDialog datePickerDialog = new DatePickerDialog(HomeScreenActivity.this, R.style.MyDatePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {


                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        txtCalendar.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                    }
                }, year, month, date);

                datePickerDialog.setTitle("Select Date");
                datePickerDialog.setIcon(R.drawable.date);
                dialog = datePickerDialog;
                dialog.show();

            }

        });


    }


    @Override
    protected void onStart() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {


                new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle)
                        .setMessage("Do you want to Exit?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                /*stopService(new Intent(HomeScreen.this,WebSocketService.class));*/
                                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                        new ResultCallback<Status>() {
                                            @Override
                                            public void onResult(Status status) {
                                                finish();
                                            }
                                        });
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                arg0.dismiss();
                            }
                        })
                        .show();


        }
        return super.onKeyDown(keyCode, event);
    }



    public void prepareNsend() {
        logoutDialog = new Dialog(HomeScreenActivity.this);
        logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutDialog.setCancelable(true);
        logoutDialog.setContentView(R.layout.prepare_dialog);

        logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        logoutOk = (Button) logoutDialog.findViewById(R.id.btnPreYes);
        logoutCancel = (Button) logoutDialog.findViewById(R.id.btnPreNo);

        logoutOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                logoutDialog.dismiss();
                ExportDataBean exportDataBean = new ExportDataBean(customer, address, date, Cname, Cphone,
                        engineerName, engineerPhone, complaintNo, capacity, model,
                        serialNo, type, remark, inputVoltage, period, serviceType,
                        inputVoltage2, inputNEvoltage, dcVoltage, dcCurrent, outputVoltage,
                        outputNEvoltage, outputCurrent, eConnection, overHeating,
                        coolingFan, UPSinternal, batteryTerminal, batteryCondition, UPSExternal);

                clearAllEdits();

                Intent intent = new Intent(HomeScreenActivity.this, EmailAttachmentActivity.class);
                intent.putExtra("exportDataBean", exportDataBean);
                startActivity(intent);
            }
        });

        logoutCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutDialog.dismiss();
            }
        });

        logoutDialog.show();
    }



    public void init() {

        imageDP= (CircularImageView) findViewById(R.id.imageDP);
        scrollView=findViewById(R.id.scrollView);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.floating_action_button);
        imageCustomersign = findViewById(R.id.imageCustomer);
        imageTechEngineerSign = findViewById(R.id.imageTechEngineer);
        txtCalendar = findViewById(R.id.txtDate);
        txtTime = findViewById(R.id.txtTime);
        imageCalendar = findViewById(R.id.imageCalendar);
        imageTime = findViewById(R.id.imageTime);

        editCustomer = findViewById(R.id.editCustomer);
        editAddress = findViewById(R.id.editAddress);
        editCName = findViewById(R.id.editCustomerName);
        editCcontact = findViewById(R.id.editCustomerContact);
        editServiceEname = findViewById(R.id.editServiceEname);
        editEngineerContact = findViewById(R.id.editEngineerContact);
        editComplaintNo = findViewById(R.id.editComplaintno);


        editCapacity = findViewById(R.id.editCapacity);
        editModel = findViewById(R.id.editModel);
        editSerial = findViewById(R.id.editSerialNo);
        editType = findViewById(R.id.editType);
        editInputVoltage = findViewById(R.id.editInputVoltage);


        editPeriod = findViewById(R.id.editPeriod);
        editServiceType = findViewById(R.id.editServiceType);

        editInputVoltage2 = findViewById(R.id.editInputVoltage2);
        editInputNEVoltage = findViewById(R.id.editInputNEVoltage);
        editDCVoltage = findViewById(R.id.editDCVoltage);
        editDCCurrent = findViewById(R.id.editDCCurrent);
        editOutputVoltage = findViewById(R.id.editOutputVoltage);
        editOutputNEVoltage = findViewById(R.id.editOutputNEVoltage);
        editOutputCurrent = findViewById(R.id.editOutputCurrent);
        editRemark = findViewById(R.id.editremark);

        checkEConnection = findViewById(R.id.checkE_Connection);
        checkOverHeating = findViewById(R.id.checkOverHeat);
        checkCoolingFan = findViewById(R.id.checkCoolingFan);
        checkUPSinternal = findViewById(R.id.checkUPSinternal);
        checkBatteryTerminal = findViewById(R.id.checkboxBatteryTerminal);
        checkBatteryCondition = findViewById(R.id.checkBatteryCondition);
        checkUPsexternal = findViewById(R.id.checkUPSexternal);

        btn_saveNsend = findViewById(R.id.btn_saveNsend);
        btn_saveNsend.setText(Html.fromHtml("SAVE & SEND"));
    }


    public void clearAllEdits() {
        txtCalendar.setText("");
        editCustomer.setText("");
        editAddress.setText("");
        editCName.setText("");
        editCcontact.setText("");
        editServiceEname.setText("");
        editEngineerContact.setText("");
        editComplaintNo.setText("");

        editCapacity.setText("");
        editModel.setText("");
        editSerial.setText("");
        editType.setText("");
        editInputVoltage.setText("");

        editPeriod.setText("");
        editServiceType.setText("");

        editInputVoltage2.setText("");
        editInputNEVoltage.setText("");
        editDCVoltage.setText("");
        editDCCurrent.setText("");
        editOutputVoltage.setText("");
        editOutputNEVoltage.setText("");
        editOutputCurrent.setText("");
        editRemark.setText("");
        editCustomer.requestFocus();
        checkEConnection.setChecked(false);
        checkOverHeating.setChecked(false);
        checkCoolingFan.setChecked(false);
        checkUPSinternal.setChecked(false);
        checkUPsexternal.setChecked(false);
        checkBatteryCondition.setChecked(false);
        checkBatteryTerminal.setChecked(false);
        imageTechEngineerSign.setImageResource(R.drawable.white_bg);
        imageCustomersign.setImageResource(R.drawable.white_bg);
    }


    public static void showImage() {
        if (btnEngr == true) {
            btnEngr = false;
            String photoPath = Environment.getExternalStorageDirectory() + "/powerone/engineer.jpg";
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(photoPath, options);
            imageTechEngineerSign.setImageBitmap(bitmap);
        } else if (btnCust == true) {
            btnCust = false;
            String photoPath = Environment.getExternalStorageDirectory() + "/powerone/customer.jpg";
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(photoPath, options);
            imageCustomersign.setImageBitmap(bitmap);
        }
    }


}

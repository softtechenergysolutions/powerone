package com.softtech.powerone.model;

import java.io.Serializable;

/**
 * Created by Vivek on 26-Oct-18.
 */

public class ExportDataBean implements Serializable {
    private String customer,address,date,Cname,Cphone,engineerName,engineerPhone,complaintNo,capacity,model,serialNo,type;
    private String remark, inputVoltage,period,serviceType,inputVoltage2,inputNEvoltage,dcVoltage,dcCurrent,outputVoltage,outputNEvoltage,outputCurrent;
    private boolean eConnection,overHeating,coolingFan,UPSinternal,batteryTerminal,batteryCondition,UPSExternal;

    public ExportDataBean(String customer, String address, String date, String cname, String cphone, String engineerName, String engineerPhone, String complaintNo, String capacity, String model, String serialNo, String type, String remark, String inputVoltage, String period, String serviceType, String inputVoltage2, String inputNEvoltage, String dcVoltage, String dcCurrent, String outputVoltage, String outputNEvoltage, String outputCurrent, boolean eConnection, boolean overHeating, boolean coolingFan, boolean UPSinternal, boolean batteryTerminal, boolean batteryCondition, boolean UPSExternal) {
        this.customer = customer;
        this.address = address;
        this.date = date;
        Cname = cname;
        Cphone = cphone;
        this.engineerName = engineerName;
        this.engineerPhone = engineerPhone;
        this.complaintNo = complaintNo;
        this.capacity = capacity;
        this.model = model;
        this.serialNo = serialNo;
        this.type = type;
        this.remark = remark;
        this.inputVoltage = inputVoltage;
        this.period = period;
        this.serviceType = serviceType;
        this.inputVoltage2 = inputVoltage2;
        this.inputNEvoltage = inputNEvoltage;
        this.dcVoltage = dcVoltage;
        this.dcCurrent = dcCurrent;
        this.outputVoltage = outputVoltage;
        this.outputNEvoltage = outputNEvoltage;
        this.outputCurrent = outputCurrent;
        this.eConnection = eConnection;
        this.overHeating = overHeating;
        this.coolingFan = coolingFan;
        this.UPSinternal = UPSinternal;
        this.batteryTerminal = batteryTerminal;
        this.batteryCondition = batteryCondition;
        this.UPSExternal = UPSExternal;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCname() {
        return Cname;
    }

    public void setCname(String cname) {
        Cname = cname;
    }

    public String getCphone() {
        return Cphone;
    }

    public void setCphone(String cphone) {
        Cphone = cphone;
    }

    public String getEngineerName() {
        return engineerName;
    }

    public void setEngineerName(String engineerName) {
        this.engineerName = engineerName;
    }

    public String getEngineerPhone() {
        return engineerPhone;
    }

    public void setEngineerPhone(String engineerPhone) {
        this.engineerPhone = engineerPhone;
    }

    public String getComplaintNo() {
        return complaintNo;
    }

    public void setComplaintNo(String complaintNo) {
        this.complaintNo = complaintNo;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getInputVoltage() {
        return inputVoltage;
    }

    public void setInputVoltage(String inputVoltage) {
        this.inputVoltage = inputVoltage;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getInputVoltage2() {
        return inputVoltage2;
    }

    public void setInputVoltage2(String inputVoltage2) {
        this.inputVoltage2 = inputVoltage2;
    }

    public String getInputNEvoltage() {
        return inputNEvoltage;
    }

    public void setInputNEvoltage(String inputNEvoltage) {
        this.inputNEvoltage = inputNEvoltage;
    }

    public String getDcVoltage() {
        return dcVoltage;
    }

    public void setDcVoltage(String dcVoltage) {
        this.dcVoltage = dcVoltage;
    }

    public String getDcCurrent() {
        return dcCurrent;
    }

    public void setDcCurrent(String dcCurrent) {
        this.dcCurrent = dcCurrent;
    }

    public String getOutputVoltage() {
        return outputVoltage;
    }

    public void setOutputVoltage(String outputVoltage) {
        this.outputVoltage = outputVoltage;
    }

    public String getOutputNEvoltage() {
        return outputNEvoltage;
    }

    public void setOutputNEvoltage(String outputNEvoltage) {
        this.outputNEvoltage = outputNEvoltage;
    }

    public String getOutputCurrent() {
        return outputCurrent;
    }

    public void setOutputCurrent(String outputCurrent) {
        this.outputCurrent = outputCurrent;
    }

    public boolean iseConnection() {
        return eConnection;
    }

    public void seteConnection(boolean eConnection) {
        this.eConnection = eConnection;
    }

    public boolean isOverHeating() {
        return overHeating;
    }

    public void setOverHeating(boolean overHeating) {
        this.overHeating = overHeating;
    }

    public boolean isCoolingFan() {
        return coolingFan;
    }

    public void setCoolingFan(boolean coolingFan) {
        this.coolingFan = coolingFan;
    }

    public boolean isUPSinternal() {
        return UPSinternal;
    }

    public void setUPSinternal(boolean UPSinternal) {
        this.UPSinternal = UPSinternal;
    }

    public boolean isBatteryTerminal() {
        return batteryTerminal;
    }

    public void setBatteryTerminal(boolean batteryTerminal) {
        this.batteryTerminal = batteryTerminal;
    }

    public boolean isBatteryCondition() {
        return batteryCondition;
    }

    public void setBatteryCondition(boolean batteryCondition) {
        this.batteryCondition = batteryCondition;
    }

    public boolean isUPSExternal() {
        return UPSExternal;
    }

    public void setUPSExternal(boolean UPSExternal) {
        this.UPSExternal = UPSExternal;
    }
}

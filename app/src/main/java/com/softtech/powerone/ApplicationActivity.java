package com.softtech.powerone;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.support.design.widget.Snackbar;
import android.view.View;





/**
 * Created by praveen on 11/10/18.
 */

public class ApplicationActivity extends Application {

    static ApplicationActivity applicationInstance;
    /***************************************************
     *
     * Declare all the Applicationactivity 's variables
     *
     ***************************************************/
    private static Context context;
    public Snackbar snackbar;
    public ProgressDialog progress,horiProgress;


    public static synchronized ApplicationActivity getInstance() {
        return applicationInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        applicationInstance = this;
    }


    /***************************************************************************
     *
     * NAME : showSnackToast
     *
     * DESCRIPTION : function uses to show the snackbar
     *
     * @param getMessage - message of the snackbar
     * @param activity - reference activity
     *
     ***************************************************************************/
    public void showSnackToast(String getMessage, Activity activity) {

        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }

        final View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        snackbar = Snackbar.make(rootView, getMessage, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.parseColor("#1873cc"));
        snackbar.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        snackbar.show();
    }

    /***************************************************************************
     *
     * NAME : showSnackBar
     *
     * DESCRIPTION : function uses to show the snackbar
     *
     * @param getMessage - message of the snackbar
     * @param activity - reference activity
     *
     ***************************************************************************/
    public void showSnackBar(String getMessage, Activity activity) {

        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }

        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        snackbar = Snackbar.make(rootView, getMessage, Snackbar.LENGTH_INDEFINITE);
       /* snackbar.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });*/
        snackbar.show();
    }

    /***************************************************************************
     *
     * NAME : dismissSnackbar
     *
     * DESCRIPTION : function uses to dismiss the snackbar
     *
     ***************************************************************************/
    public void dismissSnackbar() {
        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }


    /***************************************************************************
     *
     * NAME : showProgress
     *
     * DESCRIPTION : function uses to show progress loader
     *
     * @param message - message of the progress bar
     * @param activity - reference activity
     *
     ***************************************************************************/
    public void showProgress(String message, Activity activity) {

        progress = new ProgressDialog(activity,R.style.AppCompatAlertDialogStyle);
        progress.setMessage(message);
        progress.show();
    }

    /***************************************************************************
     *
     * NAME : dismissProgress
     *
     * DESCRIPTION : function uses to dismiss the snackbar
     *
     ***************************************************************************/
    public void dismissProgress() {
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
        }
    }




}
